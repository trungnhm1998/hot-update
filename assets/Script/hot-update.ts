const { ccclass, property } = cc._decorator;

const customManifestStr = JSON.stringify({
  "packageUrl": "http://192.168.50.220:5555/tutorial-hot-update/remote-assets/",
  "remoteManifestUrl": "http://192.168.50.220:5555/tutorial-hot-update/remote-assets/project.manifest",
  "remoteVersionUrl": "http://192.168.50.220:5555/tutorial-hot-update/remote-assets/version.manifest",
  "version": "1.10",
  "assets": {
    "src/cocos2d-jsb.js": {
      "size": 3341465,
      "md5": "fafdde66bd0a81d1e096799fb8b7af95"
    }
  },
  "searchPaths": []
});

@ccclass
export default class HotUpdate extends cc.Component {
  @property(cc.Label)
  infoLabel: cc.Label = null;

  @property(cc.Label)
  fileProgressLabel: cc.Label = null;

  @property(cc.Label)
  byteProgressLabel: cc.Label = null;

  @property(cc.Label)
  fileLabel: cc.Label = null;

  @property(cc.Label)
  byteLabel: cc.Label = null;

  @property(cc.Button)
  checkButton: cc.Button = null;

  @property(cc.Button)
  updateButton: cc.Button = null;

  @property(cc.Button)
  retryButton: cc.Button = null;

  @property(cc.Asset)
  mAssetUrl: cc.Asset = null;

  _storagePath: string = "";
  _assetManager: jsb.AssetManager = null;
  _updating: boolean = false;
  _canRetry: boolean = false;
  _updateListener: any = null;
  _failCount: number = - 1;
  // static _instance: HotUpdate = null;


  onLoad() {
    if (!cc.sys.isNative) {
      return;
    }
    this.retryButton.interactable = false;
    // HotUpdate._instance = this;

    // get path
    this._storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + "trung-remote-asset");
    cc.log("storage path for remote asset: " + this._storagePath);

    var url = this.mAssetUrl;
    // md5 security
    if (cc.loader.md5Pipe) {
      url = cc.loader.md5Pipe.transformURL(url);
    }
    this._assetManager = new jsb.AssetsManager(url, this._storagePath, this.versionCompare.bind(this));

    // Setup the verification callback, but we don't have md5 check function yet, so only print some message
    // Return true if the verification passed, otherwise return false
    this._assetManager.setVerifyCallback(this.onAssetManagerVerify.bind(this));

    this.infoLabel.string = "Hot update is ready, please check or directly update.";

    if (cc.sys.os === cc.sys.OS_ANDROID) {
      // Some Android device may slow down the download process when concurrent tasks is too much.
      // The value may not be accurate, please do more test and find what's most suitable for your game.
      this._assetManager.setMaxConcurrentTask(2);
      this.infoLabel.string = "Max concurrent tasks count have been limited to 2";
    }

    this.fileProgressLabel.string = `${0}`;
    this.byteProgressLabel.string = `${0}`;
  }

  onPressedCheckUpdate(event) {
    if (this._updating) {
      this.infoLabel.string = 'Checking or updating ...';
      return;
    }
    if (this._assetManager.getState() === jsb.AssetsManager.State.UNINITED) {
      // Resolve md5 url
      var url = this.mAssetUrl;
      if (cc.loader.md5Pipe) {
        url = cc.loader.md5Pipe.transformURL(url);
      }
      this._assetManager.loadLocalManifest(url);
    }
    if (!this._assetManager.getLocalManifest() || !this._assetManager.getLocalManifest().isLoaded()) {
      this.infoLabel.string = 'Failed to load local manifest ...';
      return;
    }
    this._assetManager.setEventCallback(this.onCheckUpdate.bind(this));

    this._assetManager.checkUpdate();
    this._updating = true;
  }

  onPressedUpdate() {
    this.retryButton.interactable = false;
    if (this._assetManager && !this._updating) {
      this._assetManager.setEventCallback(this.onUpdate.bind(this));

      // assetManager hasn't init
      if (this._assetManager.getState() === jsb.AssetsManager.State.UNINITED) {
        // Resolve md5 url
        var url = this.mAssetUrl;
        // md5 security
        if (cc.loader.md5Pipe) {
          url = cc.loader.md5Pipe.transformURL(url);
        }
        this._assetManager.loadLocalManifest(url);
      }

      this._failCount = 0;
      this._assetManager.update();
      this.updateButton.interactable = false;
      this._updating = true;
    }
  }

  onPressedRetry() {
    if (!this._updating && this._canRetry) {
      this.retryButton.interactable = false;
      this._canRetry = false;

      this.infoLabel.string = 'Retry failed Assets...';
      this._assetManager.downloadFailedAssets();
    }
  }

  onUpdate(event) {
    cc.log("onUpdate callback", event);
    var needRestart = false;
    var failed = false;
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        this.infoLabel.string = 'No local manifest file found, hot update skipped.';
        failed = true;
        break;
      case jsb.EventAssetsManager.UPDATE_PROGRESSION:
        this.byteProgressLabel.string = `${event.getPercent()}`;
        this.fileProgressLabel.string = `${event.getPercentByFile()}`;

        this.fileLabel.string = event.getDownloadedFiles() + ' / ' + event.getTotalFiles();
        this.byteLabel.string = event.getDownloadedBytes() + ' / ' + event.getTotalBytes();

        var msg = event.getMessage();
        if (msg) {
          this.infoLabel.string = 'Updated file: ' + msg;
          // cc.log(event.getPercent()/100 + '% : ' + msg);
        }
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        this.infoLabel.string = 'Fail to download manifest file, hot update skipped.';
        failed = true;
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        this.infoLabel.string = 'Already up to date with the latest remote version.';
        failed = true;
        break;
      case jsb.EventAssetsManager.UPDATE_FINISHED:
        this.infoLabel.string = 'Update finished. ' + event.getMessage();
        needRestart = true;
        break;
      case jsb.EventAssetsManager.UPDATE_FAILED:
        this.infoLabel.string = 'Update failed. ' + event.getMessage();
        this.retryButton.interactable = true;
        this._updating = false;
        this._canRetry = true;
        break;
      case jsb.EventAssetsManager.ERROR_UPDATING:
        this.infoLabel.string = 'Asset update error: ' + event.getAssetId() + ', ' + event.getMessage();
        break;
      case jsb.EventAssetsManager.ERROR_DECOMPRESS:
        this.infoLabel.string = event.getMessage();
        break;
      default:
        break;
    }

    if (failed) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
      this._updating = false;
    }

    if (needRestart) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
      // Prepend the manifest's search path
      var searchPaths = jsb.fileUtils.getSearchPaths();
      var newPaths = this._assetManager.getLocalManifest().getSearchPaths();
      console.log(JSON.stringify(newPaths));
      Array.prototype.unshift.apply(searchPaths, newPaths);
      // This value will be retrieved and appended to the default search path during game startup,
      // please refer to samples/js-tests/main.js for detailed usage.
      // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
      cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
      jsb.fileUtils.setSearchPaths(searchPaths);

      cc.audioEngine.stopAll();
      cc.game.restart();
    }
  }

  onAssetManagerVerify(path: string, asset: any) {
    // When asset is compressed, we don't need to check its md5, because zip file have been deleted.
    var compressed = asset.compressed;
    // Retrieve the correct md5 value.
    var expectedMD5 = asset.md5;
    // asset.path is relative path and path is absolute.
    var relativePath = asset.path;
    // The size of asset file, but this value could be absent.
    var size = asset.size;
    if (compressed) {
      this.infoLabel.string = "Verification passed : " + relativePath + " - size: " + size;
      return true;
    }
    else {
      this.infoLabel.string = "Verification passed : " + relativePath + ' (' + expectedMD5 + ')' + " - size: " + size;
      return true;
    }
  }

  onCheckUpdate(event) {
    cc.log('Code: ' + event.getEventCode(), jsb.EventAssetsManager);
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        this.infoLabel.string = "No local manifest file found, hot update skipped.";
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        this.infoLabel.string = "Fail to download manifest file, hot update skipped.";
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        this.infoLabel.string = "Already up to date with the latest remote version.";
        break;
      case jsb.EventAssetsManager.NEW_VERSION_FOUND:
        this.infoLabel.string = 'New version found, please try to update.';
        this.checkButton.interactable = false;
        this.fileProgressLabel.string = `${0}`;
        this.byteProgressLabel.string = `${0}`;
        break;
      default:
        return;
    }

    this._assetManager.setEventCallback(null);
    // this._checkListener = null;
    this._updating = false;
  }

  onPressedLoadManifest() {
    cc.log("HotUpdate::onPressedLoadManifest", this._assetManager.getState(), jsb.AssetsManager.State);
    if (this._assetManager.getState() === jsb.AssetsManager.State.UNINITED) {
      var manifest = new jsb.Manifest(customManifestStr, this._storagePath);
      this._assetManager.loadLocalManifest(manifest, this._storagePath);
      this.infoLabel.string = 'Using custom manifest';
    }
  }

  /**
   * Setup your own version compare handler, versionA and B is versions in string
   * if the return value greater than 0, versionA is greater than B,
   * if the return value equals 0, versionA equals to B,
   * if the return value smaller than 0, versionA is smaller than B.
   * @param versionA 
   * @param versionB 
   */
  versionCompare(versionA: string, versionB: string) {
    cc.log("JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
    let vA = versionA.split('.');
    let vB = versionB.split('.');
    for (let i = 0; i < vA.length; ++i) {
      let a = parseInt(vA[i]);
      let b = parseInt(vB[i] || '0');
      if (a === b) {
        continue;
      }
      else {
        return a - b;
      }
    }
    if (vB.length > vA.length) {
      return -1;
    }
    else {
      return 0;
    }
  }

  onDestroy() {
    if (this._updateListener) {
      this._assetManager.setEventCallback(null);
      this._updateListener = null;
    }
  }
}
